;;; boxy-info.el --- View info manuals in a boxy diagram -*- lexical-binding: t -*-

;; Copyright (C) 2021-2025 Free Software Foundation, Inc.

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Version: 1.1.3
;; File: boxy-info.el
;; Package-Requires: ((emacs "26.1") (boxy "2.0"))
;; Keywords: tools
;; URL: https://gitlab.com/grinn.amy/boxy-info

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; The command `boxy-info' will display the current Info manual as a
;; boxy diagram.  If not looking at an Info manual, boxy-info will
;; prompt for a manual to display.
;;
;; Standard Emacs movement commands will navigate by boxes instead of
;; words or characters.  Clicking on a box or pressing RET will take
;; you to that node in the manual.  If a box has a double outline, it
;; can be expanded by hovering over it with the cursor and pressing
;; TAB.

;;; Code:

;;;; Requirements

(require 'boxy)
(require 'info)
(require 'cl-lib)

;;;; Options

(defgroup boxy-info nil
  "Customization options for boxy-info."
  :group 'applications)

(defcustom boxy-info-margin-x 2
  "Horizontal margin to be used when displaying boxes."
  :type 'number)

(defcustom boxy-info-margin-y 0
  "Vertical margin to be used when displaying boxes."
  :type 'number)

(defcustom boxy-info-padding-x 2
  "Horizontal padding to be used when displaying boxes."
  :type 'number)

(defcustom boxy-info-padding-y 0
  "Vertical padding to be used when displaying boxes."
  :type 'number)

(defcustom boxy-info-include-context t
  "Whether to show context when opening a real link."
  :type 'boolean)

(defcustom boxy-info-flex-width 80
  "When merging links, try to keep width below this."
  :type 'number)

(defcustom boxy-info-default-visibility 1
  "Default level to display boxes."
  :type 'number)

(defcustom boxy-info-max-visibility 2
  "Maximum visibility to show when cycling global visibility."
  :type 'number)

(defcustom boxy-info-tooltips t
  "Show tooltips in a boxy diagram."
  :type 'boolean)

(defcustom boxy-info-tooltip-timeout 0.5
  "Idle time before showing tooltip in a boxy diagram."
  :type 'number)

(defcustom boxy-info-tooltip-max-width 30
  "Maximum width of all tooltips."
  :type 'number)

;;;; Faces

(defface boxy-info-default nil
  "Default face used in boxy mode.")

(defface boxy-info-primary
  '((((background dark)) (:foreground "turquoise"))
    (t (:foreground "dark cyan")))
  "Face for highlighting the name of a box.")

(defface boxy-info-selected
  '((t :foreground "light slate blue"))
  "Face for the current box border under cursor.")

(defface boxy-info-rel
  '((t :foreground "hot pink"))
  "Face for the box which is related to the box under the cursor.")

(defface boxy-info-tooltip
  '((((background dark)) (:background "gray30" :foreground "gray"))
    (t (:background "gainsboro" :foreground "dim gray")))
  "Face for tooltips in a boxy diagram.")

;;;; Types

(cl-defstruct boxy-info-item name children)

;;;; Pretty printing

(cl-defun boxy-info-pp (box
                        &key
                        buffer
                        (display-buffer-fn 'display-buffer-pop-up-window)
                        (visibility boxy-info-default-visibility)
                        (max-visibility boxy-info-max-visibility)
                        select
                        header
                        (default-margin-x boxy-info-margin-x)
                        (default-margin-y boxy-info-margin-y)
                        (default-padding-x boxy-info-padding-x)
                        (default-padding-y boxy-info-padding-y)
                        (flex-width boxy-info-flex-width)
                        (tooltips boxy-info-tooltips)
                        (tooltip-timeout boxy-info-tooltip-timeout)
                        (tooltip-max-width boxy-info-tooltip-max-width)
                        (default-face 'boxy-info-default)
                        (primary-face 'boxy-info-primary)
                        (tooltip-face 'boxy-info-tooltip)
                        (rel-face 'boxy-info-rel)
                        (selected-face 'boxy-info-selected))
  "Pretty print BOX in a popup buffer.

BUFFER can be the buffer to display the boxy diagram in, otherwise it
will always use the *Boxy* buffer.

If HEADER is passed in, it will be printed above the diagram.

DISPLAY-BUFFER-FN is used to display the diagram, by
default `display-buffer-pop-up-window'.

If SELECT is non-nil, select the boxy window after displaying
it.

VISIBILITY is the initial visibility of children and
MAX-VISIBILITY is the maximum depth to display when cycling
visibility.

DEFAULT-MARGIN-X, DEFAULT-MARGIN-Y, DEFAULT-PADDING-X and
DEFAULT-PADDING-Y will be the fallback values to use if a box's
margin and padding slots are not set.

When adding boxes, boxy will try to keep the width below
FLEX-WIDTH.

If TOOLTIPS is nil, don't show any tooltips.

TOOLTIP-TIMEOUT is the idle time to wait before showing a
tooltip.

TOOLTIP-MAX-WIDTH is the maximum width of a tooltip.  Lines
longer than this will be truncated.

DEFAULT-FACE, PRIMARY-FACE, TOOLTIP-FACE, REL-FACE, and
SELECTED-FACE can be set to change the appearance of the boxy
diagram."
  (boxy-pp box
           :buffer buffer
           :display-buffer-fn display-buffer-fn
           :visibility visibility
           :max-visibility max-visibility
           :select select
           :header header
           :default-margin-x default-margin-x
           :default-margin-y default-margin-y
           :default-padding-x default-padding-x
           :default-padding-y default-padding-y
           :flex-width flex-width
           :tooltips tooltips
           :tooltip-timeout tooltip-timeout
           :tooltip-max-width tooltip-max-width
           :default-face default-face
           :primary-face primary-face
           :tooltip-face tooltip-face
           :rel-face rel-face
           :selected-face selected-face))

;;;; Commands

;;;###autoload
(defun boxy-info ()
  "View current info manual as a boxy diagram.

If not looking at an info manual, prompt for one to view."
  (interactive)
  (let* ((world-box (boxy-box))
         (world-item (make-boxy-info-item))
         (current-file (if Info-current-file Info-current-file
                         (completing-read "Manual name: "
                                          (info--manual-names current-prefix-arg)
                                          nil t)))
         (current-node Info-current-node)
         (node-list (Info-toc-nodes current-file)))
    (boxy-info-add-item world-item (car node-list) node-list)
    (mapc
     (lambda (item)
       (boxy-info-add-box world-box item))
     (boxy-info-item-children world-item))
    (boxy-info-pp world-box :select t)
    (with-current-buffer (get-buffer "*Boxy*")
      (setq Info-current-file current-file)
      (when current-node
        (if-let ((path-to-current (boxy-info-find-path current-node world-item)))
            (boxy-info-expand-boxes (reverse path-to-current) world-box))))))

;;;; Utility expressions

(defun boxy-info-add-item (parent node node-list)
  "Convert NODE to an item and add it to PARENT.

NODE-LIST is a list of all nodes needed to look up child
nodes."
  (let* ((name (car node))
         (children (reverse (cadddr node)))
         (item (make-boxy-info-item :name name :children '())))
    (push item (boxy-info-item-children parent))
    (mapc
     (lambda (child)
       (if-let (child-node (seq-find
                            (lambda (el)
                              (and (string= (car el) child)
                                   (string= (cadr el) name)))
                            node-list))
           (boxy-info-add-item item child-node node-list)
         (push (make-boxy-info-item :name child)
               (boxy-info-item-children item))))
     children)))

(defun boxy-info-add-box (parent item)
  "Convert ITEM to a box and add it to PARENT."
  (let* ((name (boxy-info-item-name item))
         (box (boxy-box :name name
                        :action `(lambda ()
                                   (interactive)
                                   (let ((node-address (concat
                                                        "(" Info-current-file ")"
                                                        ,name)))
                                     (quit-window)
                                     (Info-goto-node node-address)))
                        :rel "in")))
    (when-let ((children (boxy-info-item-children item)))
      (setf (boxy-box-expand-children box)
            `((lambda (box)
                (mapc
                 (lambda (child)
                   (boxy-info-add-box box child))
                 ',children)))))
    (boxy-add-next box parent)))

(defun boxy-info-find-path (search-name item &optional previous-hierarchy)
  "Find item named SEARCH-NAME within ITEM and return the hierarchy as a list.

PREVIOUS-HIERARCHY is the existing hierarchy above the current
item, used for recursion."
  (let ((hierarchy (or previous-hierarchy '()))
        (item-name (boxy-info-item-name item))
        (children (boxy-info-item-children item)))
    (push item-name hierarchy)
    (cond
     ((string= item-name search-name)
      hierarchy)
     ((not children)
      nil)
     (t
      (catch 'found-hierarchy
        (dolist (child children)
          (if-let ((new-hierarchy (boxy-info-find-path search-name child hierarchy)))
              (throw 'found-hierarchy new-hierarchy))))))))

(defun boxy-info-expand-boxes (box-names world)
  "For each element in BOX-NAMES, find and expand the corresponding box.

The WORLD box is needed to find the boxes and update the flexible
layout.

This will also jump to the final box in BOX-NAMES."
  (catch 'not-found
    (let (parent child)
      (dolist (name box-names)
        (when name
          (setq child (boxy-find-matching (boxy-box :name name) (or parent world)))
          (when (not child)
            (boxy--expand-box parent)
            (setq child (boxy-find-matching (boxy-box :name name) (or parent world))))
          (when (not child)
            (throw 'not-found nil))
          (setq parent child)))
      (boxy--flex-adjust world world)
      (boxy-mode-redraw)
      (boxy-jump-to-box child))))

(provide 'boxy-info)
;;; boxy-info.el ends here
